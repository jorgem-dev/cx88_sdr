// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (c) 2020 Jorge Maidana <jorgem.linux@gmail.com>
 *
 * This driver is a derivative of:
 *
 * Device driver for Conexant 2388x based TV cards
 * Copyright (c) 2003 Gerd Knorr <kraxel@bytesex.org> [SuSE Labs]
 *
 * Device driver for Conexant 2388x based TV cards
 * Copyright (c) 2005-2006 Mauro Carvalho Chehab <mchehab@kernel.org>
 *
 * cxadc.c - CX2388x ADC DMA driver for Linux 2.6.18 version 0.3
 * Copyright (c) 2005-2007 Hew How Chee <how_chee@yahoo.com>
 *
 * cxadc.c - CX2388x ADC DMA driver for Linux 3.x version 0.5
 * Copyright (c) 2013-2015 Chad Page <Chad.Page@gmail.com>
 */

#include <linux/interrupt.h>
#include <linux/math64.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/videodev2.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-dev.h>
#include <media/v4l2-device.h>
#include <media/v4l2-event.h>
#include <media/v4l2-ioctl.h>

MODULE_DESCRIPTION("CX2388x SDR V4L2 Driver");
MODULE_AUTHOR("Jorge Maidana <jorgem.linux@gmail.com>");
MODULE_VERSION("1.8.0");
MODULE_LICENSE("GPL");

#define CX88SDR_DRV_NAME		"CX2388x SDR"
#define CX88SDR_MAX_CARDS		32

static int cx88sdr_devcount;

static int latency[CX88SDR_MAX_CARDS];
module_param_array(latency, int, NULL, 0);
MODULE_PARM_DESC(latency, "Set the PCI latency timer value, comma separated for each card");

static int xtal[CX88SDR_MAX_CARDS];
module_param_array(xtal, int, NULL, 0);
MODULE_PARM_DESC(xtal, "Set the crystal frequency in Hz, comma separated for each card");

#define CX88SDR_SDR_FREQ		28800000 /* Default SDR Frequency */
#define CX88SDR_8FSC_NTSC		28636363 /* 8xFsc NTSC */
#define CX88SDR_8FSC_PAL		35468950 /* 8xFsc PAL */
#define CX88SDR_XTAL_FREQ_MIN		24000000 /* Minimum Xtal Frequency */
#define CX88SDR_XTAL_FREQ_MAX		48000000 /* Maximum Xtal Frequency */
#define CX88SDR_XTAL_FREQ		CX88SDR_8FSC_NTSC /* Default Xtal Frequency */

#define CX88SDR_DEV_CNTRL2		0x200034 /* Device Control #2 */
#define CX88SDR_PCI_INT_MSK		0x200040 /* PCI Interrupt Mask */
#define CX88SDR_PCI_INT_MSK_CLEAR	0x000000 /* PCI Interrupt Mask Clear */
#define CX88SDR_PCI_INT_MSK_VAL		0x000001 /* PCI Interrupt Mask Value */
#define CX88SDR_VID_INT_MSK		0x200050 /* Video Interrupt Mask */
#define CX88SDR_VID_INT_MSK_CLEAR	0x000000 /* Video Interrupt Mask Clear */
#define CX88SDR_VID_INT_MSK_VAL		0x018888 /* Video Interrupt Mask Value */
#define CX88SDR_VID_INT_STAT		0x200054 /* Video Interrupt Status */
#define CX88SDR_VID_INT_STAT_CLEAR	0x0fffff /* Video Interrupt Status Clear */

#define CX88SDR_DMA24_PTR2		0x3000cc /* IPB DMAC Current Table Pointer */
#define CX88SDR_DMA24_CNT1		0x30010c /* IPB DMAC Buffer Limit */
#define CX88SDR_DMA24_CNT2		0x30014c /* IPB DMAC Table Size */
#define CX88SDR_VBI_GP_CNT		0x31c02c /* VBI General Purpose Counter */
#define CX88SDR_VID_DMA_CNTRL		0x31c040 /* IPB DMA Control */
#define CX88SDR_AFE_CFG_IO		0x35c04c /* ADC Mode Select */

#define CX88SDR_DEV_STAT		0x310100 /* Device Status */
#define CX88SDR_INPUT_FORMAT		0x310104 /* Input Format Register */
#define CX88SDR_HTOTAL			0x310120 /* Total Pixel Count */
#define CX88SDR_OUTPUT_FORMAT		0x310164 /* Output Format */
#define CX88SDR_PLL_REG			0x310168 /* PLL Register */
#define CX88SDR_PLL_ADJ_CTRL		0x31016c /* PLL Adjust Control Register */
#define CX88SDR_SCONV_REG		0x310170 /* Sample Rate Conversion Register */
#define CX88SDR_CAPTURE_CTRL		0x310180 /* Capture Control */
#define CX88SDR_COLOR_CTRL		0x310184 /* Color Format/Control */
#define CX88SDR_VBI_PACKET		0x310188 /* VBI Packet Size/Delay */
#define CX88SDR_AGC_TIP3		0x310210 /* AGC Sync Tip Adjust 3 */
#define CX88SDR_AGC_ADJ3		0x31021c /* AGC Gain Adjust 3 */
#define CX88SDR_AGC_ADJ4		0x310220 /* AGC Gain Adjust 4 */

#define CX88SDR_SRAM_ADDR		0x180000 /* 32 KiB SRAM */
#define CX88SDR_DMA24_CMDS_ADDR		(CX88SDR_SRAM_ADDR + 0x0100) /* DMA CH24 CMDS */
#define CX88SDR_RISC_INST_QUEUE_ADDR	(CX88SDR_SRAM_ADDR + 0x0800) /* RISC Instruction Queue */
#define CX88SDR_CDT_ADDR		(CX88SDR_SRAM_ADDR + 0x1000) /* Cluster Descriptor Table */
#define CX88SDR_CLUSTER_BUF_ADDR	(CX88SDR_SRAM_ADDR + 0x4000) /* Cluster Buffers */

#define CX88SDR_CDT_SIZE		(64 >> 3)  /* CDT 8-byte sized, 2 minimum */
#define CX88SDR_RISC_INST_QUEUE_SIZE	(256 >> 2) /* RISC Instruction Queue 4-byte sized */
#define CX88SDR_RISC_CNT_INCR		(1U << 16) /* Increment DMA Frame Slot */
#define CX88SDR_RISC_CNT_RESET		(3U << 16) /* Reset DMA Frame Slot */
#define CX88SDR_RISC_EOL		(1U << 26) /* End of Virtual VBI Scanline */
#define CX88SDR_RISC_SOL		(1U << 27) /* Start of Virtual VBI Scanline */
#define CX88SDR_RISC_WRITE		(1U << 28) /* RISC WRITE Instruction Opcode */
#define CX88SDR_RISC_JUMP		(7U << 28) /* RISC JUMP Instruction Opcode */
#define CX88SDR_RISC_SYNC		(8U << 28) /* RISC SYNC Instruction Opcode */
#define CX88SDR_RISC_INST_WRITE_SIZE	SZ_8       /* RISC WRITE Instruction Size */
#define CX88SDR_RISC_INST_JUMP_SIZE	SZ_8       /* RISC JUMP Instruction Size */
#define CX88SDR_RISC_INST_SYNC_SIZE	SZ_4       /* RISC SYNC Instruction Size */
#define CX88SDR_VBI_PACKET_SIZE		SZ_2K      /* Virtual VBI Scanline Size */
#define CX88SDR_VBI_DMA_AREA_SIZE	SZ_64M     /* DMA Area Size */
#define CX88SDR_VBI_DMA_FRAME_SIZE	(PAGE_ALIGN(CX88SDR_VBI_PACKET_SIZE))
#define CX88SDR_VBI_DMA_FRAMES		(CX88SDR_VBI_DMA_AREA_SIZE / CX88SDR_VBI_DMA_FRAME_SIZE)
#define CX88SDR_VBI_PACKETS_PER_FRAME	(CX88SDR_VBI_DMA_FRAME_SIZE / CX88SDR_VBI_PACKET_SIZE)
#define CX88SDR_RISC_BUF_SIZE_PER_FRAME	(CX88SDR_RISC_INST_WRITE_SIZE *			\
					 CX88SDR_VBI_PACKETS_PER_FRAME)
#define CX88SDR_RISC_INST_LOOP_SIZE	(CX88SDR_VBI_DMA_FRAMES * CX88SDR_RISC_BUF_SIZE_PER_FRAME)

/* SYNC + WRITEs + JUMP */
#define CX88SDR_RISC_BUF_SIZE		(PAGE_ALIGN(CX88SDR_RISC_INST_SYNC_SIZE +	\
						    CX88SDR_RISC_INST_LOOP_SIZE +	\
						    CX88SDR_RISC_INST_JUMP_SIZE))
#define CX88SDR_RISC_WRITE_VBI_PACKET	(CX88SDR_RISC_WRITE | CX88SDR_VBI_PACKET_SIZE |	\
					 CX88SDR_RISC_SOL | CX88SDR_RISC_EOL)

#define cx88sdr_pr_info(fmt, ...)	pr_info(KBUILD_MODNAME " %s: " fmt,		\
						pci_name(dev->pdev), ##__VA_ARGS__)
#define cx88sdr_pr_err(fmt, ...)	pr_err(KBUILD_MODNAME " %s: " fmt,		\
						pci_name(dev->pdev), ##__VA_ARGS__)

/* Real formats */
#ifndef V4L2_SDR_FMT_RU8
#define V4L2_SDR_FMT_RU8		V4L2_SDR_FMT_CU8
#endif
#ifndef V4L2_SDR_FMT_RU16LE
#define V4L2_SDR_FMT_RU16LE		V4L2_SDR_FMT_CU16LE
#endif

#define CX88SDR_BAND_RANGE_ALIGN	(192000U * 8)
#define CX88SDR_BAND_RANGE_LOW(f)	(rounddown((f) / 2, CX88SDR_BAND_RANGE_ALIGN))
#define CX88SDR_BAND_RANGE_HIGH(f)	(roundup((f) * 5 / 4, CX88SDR_BAND_RANGE_ALIGN))

enum {
	CX88SDR_BAND_RU08,
	CX88SDR_BAND_RU16,
};

/* Reserve 16 controls for this driver */
#ifndef V4L2_CID_USER_CX88SDR_BASE
#define V4L2_CID_USER_CX88SDR_BASE	(V4L2_CID_USER_BASE + 0x1f10)
#endif

enum {
	V4L2_CID_CX88SDR_INPUT = (V4L2_CID_USER_CX88SDR_BASE + 0), /* Pin input select (YADC_SEL) */
	V4L2_CID_CX88SDR_ADC_MODE,      /* ADC mode */
	V4L2_CID_CX88SDR_VIDEO_FORMAT,  /* Video format */
	V4L2_CID_CX88SDR_GAIN,          /* Gain control */
	V4L2_CID_CX88SDR_GAIN_6DB,      /* +6dB gain control */
	V4L2_CID_CX88SDR_AGC_TIP3,      /* AGC Sync Tip Adjust 3 */
	V4L2_CID_CX88SDR_AGC_ADJ3,      /* AGC Gain Adjust 3 */
	V4L2_CID_CX88SDR_HTOTAL,        /* Total number of pixels per line (HTOTAL) */
	V4L2_CID_CX88SDR_HLOCK,         /* Horizontal lock status (HLOCK) */
	V4L2_CID_CX88SDR_AFC_PLL,       /* PLL AFC (PLL_ADJ_EN) */
};

enum {
	CX88SDR_CTRL_SDR,
	CX88SDR_CTRL_NTSC,
	CX88SDR_CTRL_PAL,
};

#define CX88SDR_CTRL_MODE		CX88SDR_CTRL_SDR

struct cx88sdr_ctrl {
	u32				freq;
	u32				pixelformat;
	u32				buffersize;
	s32				input;
	s32				video_format;
	s32				gain;
	s32				agc_tip3;
	s32				agc_adj3;
	s32				htotal;
	bool				gain_6db;
	bool				afc_pll;
	u8				reserved[26];
};

struct cx88sdr_dev {
	s32				nr;
	s32				xtal;
	char				name[32];

	/* IO */
	struct	pci_dev			*pdev;
	dma_addr_t			risc_buf_addr;
	dma_addr_t			*dma_frames_addr;
	u32	__iomem			*ctrl;
	__le32				*risc_buf;
	void				**dma_frames;
	u32				irq;
	u32				pci_lat;

	/* V4L2 */
	struct	v4l2_device		v4l2_dev;
	struct	v4l2_ctrl_handler	ctrl_handler;
	struct	v4l2_frequency_band	band[2];
	struct	video_device		vdev;
	struct	mutex			vdev_mlock;
	struct	mutex			vopen_mlock;
	struct	cx88sdr_ctrl		vctrl;
	u32				vopen;
};

struct cx88sdr_fh {
	struct v4l2_fh fh;
	struct cx88sdr_dev *dev;
	u32 sframe;
};

/* Initial control values */
static const struct cx88sdr_ctrl cx88sdr_ctrl_mode[] = {
	[CX88SDR_CTRL_SDR]  = { CX88SDR_SDR_FREQ, V4L2_SDR_FMT_RU8, CX88SDR_VBI_DMA_FRAME_SIZE,
				0, 2, 0, 56, 0, 1024, 1, 1, { 0 } }, /* 0 IRE Setup */
	[CX88SDR_CTRL_NTSC] = { CX88SDR_8FSC_NTSC, V4L2_SDR_FMT_RU8, CX88SDR_VBI_DMA_FRAME_SIZE,
				0, 1, 0,  0, 0,  910, 0, 1, { 0 } },
	[CX88SDR_CTRL_PAL]  = { CX88SDR_8FSC_PAL, V4L2_SDR_FMT_RU8, CX88SDR_VBI_DMA_FRAME_SIZE,
				0, 4, 0,  0, 0, 1135, 0, 1, { 0 } },
	{ }
};

static __always_inline u32 cx88sdr_ioread(struct cx88sdr_dev *dev, u32 reg)
{
	return ioread32(dev->ctrl + (reg >> 2));
}

static __always_inline void cx88sdr_iowrite(struct cx88sdr_dev *dev, u32 reg, u32 val)
{
	iowrite32(val, dev->ctrl + (reg >> 2));
}

static int cx88sdr_open(struct file *file)
{
	struct video_device *vdev = video_devdata(file);
	struct cx88sdr_dev *dev = container_of(vdev, struct cx88sdr_dev, vdev);
	struct cx88sdr_fh *fh;
	u32 cframe;

	fh = kzalloc(sizeof(*fh), GFP_KERNEL);
	if (!fh)
		return -ENOMEM;
	v4l2_fh_init(&fh->fh, vdev);
	fh->dev = dev;
	file->private_data = &fh->fh;
	v4l2_fh_add(&fh->fh);
	cframe = cx88sdr_ioread(dev, CX88SDR_VBI_GP_CNT);
	cframe = cframe ? (cframe - 1) : (CX88SDR_VBI_DMA_FRAMES - 1);
	WARN_ON_ONCE(cframe >= CX88SDR_VBI_DMA_FRAMES);
	fh->sframe = cframe;
	mutex_lock(&dev->vopen_mlock);
	if (!dev->vopen++)
		cx88sdr_iowrite(dev, CX88SDR_PCI_INT_MSK, CX88SDR_PCI_INT_MSK_VAL);
	mutex_unlock(&dev->vopen_mlock);
	return 0;
}

static int cx88sdr_release(struct file *file)
{
	struct v4l2_fh *vfh = file->private_data;
	struct cx88sdr_fh *fh = container_of(vfh, struct cx88sdr_fh, fh);
	struct cx88sdr_dev *dev = fh->dev;

	mutex_lock(&dev->vopen_mlock);
	if (!--dev->vopen)
		cx88sdr_iowrite(dev, CX88SDR_PCI_INT_MSK, CX88SDR_PCI_INT_MSK_CLEAR);
	mutex_unlock(&dev->vopen_mlock);
	v4l2_fh_del(&fh->fh);
	v4l2_fh_exit(&fh->fh);
	kfree(fh);
	return 0;
}

static ssize_t cx88sdr_read(struct file *file, char __user *buf, size_t size, loff_t *pos)
{
	struct v4l2_fh *vfh = file->private_data;
	struct cx88sdr_fh *fh = container_of(vfh, struct cx88sdr_fh, fh);
	struct cx88sdr_dev *dev = fh->dev;
	ssize_t result = 0;
	u32 frame, cframe;

	frame = (fh->sframe + ((*pos % CX88SDR_VBI_DMA_AREA_SIZE) / CX88SDR_VBI_DMA_FRAME_SIZE)) %
		CX88SDR_VBI_DMA_FRAMES;

retry:
	do {
		cframe = cx88sdr_ioread(dev, CX88SDR_VBI_GP_CNT);
		cframe = cframe ? (cframe - 1) : (CX88SDR_VBI_DMA_FRAMES - 1);
		if ((frame == cframe) && (file->f_flags & O_NONBLOCK))
			return -EAGAIN;
		cpu_relax();
	} while (size && (frame == cframe));

	while (size && (frame != cframe)) {
		u32 len;

		/* Handle partial frames */
		len = (*pos % CX88SDR_VBI_DMA_FRAME_SIZE) ?
		      (CX88SDR_VBI_DMA_FRAME_SIZE - (*pos % CX88SDR_VBI_DMA_FRAME_SIZE)) :
		       CX88SDR_VBI_DMA_FRAME_SIZE;
		if (len > size)
			len = size;

		if (copy_to_user(buf, dev->dma_frames[frame] +
				(*pos % CX88SDR_VBI_DMA_FRAME_SIZE), len))
			return -EFAULT;

		result += len;
		buf += len;
		*pos += len;
		size -= len;
		frame = (fh->sframe + ((*pos % CX88SDR_VBI_DMA_AREA_SIZE) /
			CX88SDR_VBI_DMA_FRAME_SIZE)) % CX88SDR_VBI_DMA_FRAMES;
	}

	if (size && !(file->f_flags & O_NONBLOCK))
		goto retry;

	return result;
}

static __poll_t cx88sdr_poll(struct file *file, struct poll_table_struct *wait)
{
	return (EPOLLIN | EPOLLRDNORM | v4l2_ctrl_poll(file, wait));
}

static const struct v4l2_file_operations cx88sdr_fops = {
	.owner		= THIS_MODULE,
	.open		= cx88sdr_open,
	.release	= cx88sdr_release,
	.read		= cx88sdr_read,
	.poll		= cx88sdr_poll,
	.unlocked_ioctl	= video_ioctl2,
};

static int cx88sdr_querycap(struct file *file, void __always_unused *priv,
			    struct v4l2_capability *cap)
{
	struct cx88sdr_dev *dev = video_drvdata(file);

	snprintf(cap->bus_info, sizeof(cap->bus_info), "PCI:%s", pci_name(dev->pdev));
	strscpy(cap->card, CX88SDR_DRV_NAME, sizeof(cap->card));
	strscpy(cap->driver, KBUILD_MODNAME, sizeof(cap->driver));
	return 0;
}

static int cx88sdr_enum_fmt_sdr(struct file __always_unused *file, void __always_unused *priv,
				struct v4l2_fmtdesc *f)
{
	switch (f->index) {
	case 0:
		f->pixelformat = V4L2_SDR_FMT_RU8;
		break;
	case 1:
		f->pixelformat = V4L2_SDR_FMT_RU16LE;
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static int cx88sdr_try_fmt_sdr(struct file *file, void __always_unused *priv, struct v4l2_format *f)
{
	struct cx88sdr_dev *dev = video_drvdata(file);

	memset(f->fmt.sdr.reserved, 0, sizeof(f->fmt.sdr.reserved));
	if (f->fmt.sdr.pixelformat != V4L2_SDR_FMT_RU8 &&
	    f->fmt.sdr.pixelformat != V4L2_SDR_FMT_RU16LE)
		f->fmt.sdr.pixelformat = V4L2_SDR_FMT_RU8;
	f->fmt.sdr.buffersize = dev->vctrl.buffersize;
	return 0;
}

static int cx88sdr_g_fmt_sdr(struct file *file, void __always_unused *priv, struct v4l2_format *f)
{
	struct cx88sdr_dev *dev = video_drvdata(file);

	memset(f->fmt.sdr.reserved, 0, sizeof(f->fmt.sdr.reserved));
	f->fmt.sdr.pixelformat = dev->vctrl.pixelformat;
	f->fmt.sdr.buffersize = dev->vctrl.buffersize;
	return 0;
}

static int cx88sdr_adc_fmt_set(struct cx88sdr_dev *dev)
{
	s64 pll_frac, sconv_val, freq = dev->vctrl.freq;
	u32 pll_int, pll_freq;

	switch (dev->vctrl.pixelformat) {
	case V4L2_SDR_FMT_RU8:
		freq = clamp_t(s64, freq, dev->band[CX88SDR_BAND_RU08].rangelow,
					  dev->band[CX88SDR_BAND_RU08].rangehigh);
		dev->vctrl.freq = (u32)freq;
		pll_freq = dev->vctrl.freq;
		cx88sdr_iowrite(dev, CX88SDR_CAPTURE_CTRL, 0x46);
		break;
	case V4L2_SDR_FMT_RU16LE:
		freq = clamp_t(s64, freq, dev->band[CX88SDR_BAND_RU16].rangelow,
					  dev->band[CX88SDR_BAND_RU16].rangehigh);
		dev->vctrl.freq = (u32)freq;
		pll_freq = dev->vctrl.freq * 2;
		cx88sdr_iowrite(dev, CX88SDR_CAPTURE_CTRL, 0x66);
		break;
	default:
		return -EINVAL;
	}

	/* (Xtal / 4 / 8) * (pll_int + (pll_frac / 2^20)) = pll_freq */
	for (pll_int = 14; pll_int < 64; pll_int++) {
		pll_frac = div_s64(pll_freq * 0x2000000LL, dev->xtal) - (pll_int << 20);
		if (pll_frac <= 0xfffff)
			break;
	}

	if ((pll_frac < 0) || (pll_frac > 0xfffff)) {
		cx88sdr_pr_err("frequency %lld Hz out of range, PLL frac = %lld\n", freq, pll_frac);
		return -EINVAL;
	}

	/* (Xtal / pll_freq) * 2^17 = sconv_val */
	sconv_val = div_s64(dev->xtal * 0x20000LL, (s32)pll_freq);
	if ((sconv_val < 0) || (sconv_val > 0x7ffff)) {
		cx88sdr_pr_err("frequency %lld Hz out of range, SCV val = %lld\n", freq, sconv_val);
		return -EINVAL;
	}

	cx88sdr_iowrite(dev, CX88SDR_SCONV_REG, (u32)sconv_val);
	cx88sdr_iowrite(dev, CX88SDR_PLL_REG, 0x08000000 | (pll_int << 20) | (u32)pll_frac);
	return 0;
}

static int cx88sdr_s_fmt_sdr(struct file *file, void __always_unused *priv, struct v4l2_format *f)
{
	struct cx88sdr_dev *dev = video_drvdata(file);

	memset(f->fmt.sdr.reserved, 0, sizeof(f->fmt.sdr.reserved));
	if (f->fmt.sdr.pixelformat != V4L2_SDR_FMT_RU8 &&
	    f->fmt.sdr.pixelformat != V4L2_SDR_FMT_RU16LE)
		f->fmt.sdr.pixelformat = V4L2_SDR_FMT_RU8;

	if ((f->fmt.sdr.pixelformat == V4L2_SDR_FMT_RU8) &&
	    (dev->vctrl.pixelformat == V4L2_SDR_FMT_RU16LE))
		dev->vctrl.freq = dev->vctrl.freq * 2;
	else if ((f->fmt.sdr.pixelformat == V4L2_SDR_FMT_RU16LE) &&
		 (dev->vctrl.pixelformat == V4L2_SDR_FMT_RU8))
		dev->vctrl.freq = dev->vctrl.freq / 2;

	dev->vctrl.pixelformat = f->fmt.sdr.pixelformat;
	f->fmt.sdr.buffersize = dev->vctrl.buffersize;
	return cx88sdr_adc_fmt_set(dev);
}

static int cx88sdr_g_tuner(struct file *file, void __always_unused *priv, struct v4l2_tuner *t)
{
	struct cx88sdr_dev *dev = video_drvdata(file);

	if (t->index > 0)
		return -EINVAL;

	switch (dev->vctrl.pixelformat) {
	case V4L2_SDR_FMT_RU8:
		t->type = dev->band[CX88SDR_BAND_RU08].type;
		t->capability = dev->band[CX88SDR_BAND_RU08].capability;
		t->rangelow = dev->band[CX88SDR_BAND_RU08].rangelow;
		t->rangehigh = dev->band[CX88SDR_BAND_RU08].rangehigh;
		break;
	case V4L2_SDR_FMT_RU16LE:
		t->type = dev->band[CX88SDR_BAND_RU16].type;
		t->capability = dev->band[CX88SDR_BAND_RU16].capability;
		t->rangelow = dev->band[CX88SDR_BAND_RU16].rangelow;
		t->rangehigh = dev->band[CX88SDR_BAND_RU16].rangehigh;
		break;
	default:
		return -EINVAL;
	}
	strscpy(t->name, "ADC: CX2388x SDR", sizeof(t->name));
	return 0;
}

static int cx88sdr_s_tuner(struct file __always_unused *file, void __always_unused *priv,
			   const struct v4l2_tuner *t)
{
	if (t->index > 0)
		return -EINVAL;
	return 0;
}

static int cx88sdr_enum_freq_bands(struct file *file, void __always_unused *priv,
				   struct v4l2_frequency_band *band)
{
	struct cx88sdr_dev *dev = video_drvdata(file);

	if (band->tuner > 0 || band->index > 0)
		return -EINVAL;

	switch (dev->vctrl.pixelformat) {
	case V4L2_SDR_FMT_RU8:
		*band = dev->band[CX88SDR_BAND_RU08];
		break;
	case V4L2_SDR_FMT_RU16LE:
		*band = dev->band[CX88SDR_BAND_RU16];
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static int cx88sdr_g_frequency(struct file *file, void __always_unused *priv,
			       struct v4l2_frequency *f)
{
	struct cx88sdr_dev *dev = video_drvdata(file);

	if (f->tuner > 0)
		return -EINVAL;

	switch (dev->vctrl.pixelformat) {
	case V4L2_SDR_FMT_RU8:
		f->frequency = dev->vctrl.freq;
		f->type = dev->band[CX88SDR_BAND_RU08].type;
		break;
	case V4L2_SDR_FMT_RU16LE:
		f->frequency = dev->vctrl.freq;
		f->type = dev->band[CX88SDR_BAND_RU16].type;
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static int cx88sdr_s_frequency(struct file *file, void __always_unused *priv,
			       const struct v4l2_frequency *f)
{
	struct cx88sdr_dev *dev = video_drvdata(file);

	if (f->tuner > 0 || f->type != V4L2_TUNER_SDR)
		return -EINVAL;

	dev->vctrl.freq = f->frequency;
	return cx88sdr_adc_fmt_set(dev);
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int cx88sdr_g_register(struct file *file, void __always_unused *priv,
			      struct v4l2_dbg_register *reg)
{
	struct cx88sdr_dev *dev = video_drvdata(file);

	reg->size = sizeof(u32);
	reg->val = (__u64)cx88sdr_ioread(dev, (u32)(reg->reg & 0xfffffc));
	return 0;
}

static int cx88sdr_s_register(struct file *file, void __always_unused *priv,
			      const struct v4l2_dbg_register *reg)
{
	struct cx88sdr_dev *dev = video_drvdata(file);

	cx88sdr_iowrite(dev, (u32)(reg->reg & 0xfffffc), lower_32_bits(reg->val)); /* 24-bit regs */
	return 0;
}
#endif

static const struct v4l2_ioctl_ops cx88sdr_ioctl_ops = {
	.vidioc_querycap		= cx88sdr_querycap,
	.vidioc_enum_fmt_sdr_cap	= cx88sdr_enum_fmt_sdr,
	.vidioc_try_fmt_sdr_cap		= cx88sdr_try_fmt_sdr,
	.vidioc_g_fmt_sdr_cap		= cx88sdr_g_fmt_sdr,
	.vidioc_s_fmt_sdr_cap		= cx88sdr_s_fmt_sdr,
	.vidioc_g_tuner			= cx88sdr_g_tuner,
	.vidioc_s_tuner			= cx88sdr_s_tuner,
	.vidioc_enum_freq_bands		= cx88sdr_enum_freq_bands,
	.vidioc_g_frequency		= cx88sdr_g_frequency,
	.vidioc_s_frequency		= cx88sdr_s_frequency,
	.vidioc_log_status		= v4l2_ctrl_log_status,
	.vidioc_subscribe_event		= v4l2_ctrl_subscribe_event,
	.vidioc_unsubscribe_event	= v4l2_event_unsubscribe,
#ifdef CONFIG_VIDEO_ADV_DEBUG
	.vidioc_g_register		= cx88sdr_g_register,
	.vidioc_s_register		= cx88sdr_s_register,
#endif
};

static const struct video_device cx88sdr_vdev = {
	.device_caps	= (V4L2_CAP_READWRITE | V4L2_CAP_SDR_CAPTURE | V4L2_CAP_TUNER),
	.fops		= &cx88sdr_fops,
	.ioctl_ops	= &cx88sdr_ioctl_ops,
	.name		= CX88SDR_DRV_NAME,
	.release	= video_device_release_empty,
};

static s32 cx88sdr_adc_mode_get(struct cx88sdr_dev *dev)
{
	u32 status = cx88sdr_ioread(dev, CX88SDR_DEV_STAT);
	s32 mode = 0;

	if (((status >> 6) & 3) == 3)
		mode = ((status >> 3) & 1) ? 3 : 1;
	else if (((status >> 6) & 3) == 1)
		mode = ((status >> 3) & 1) ? 4 : 2;
	return mode;
}

static s32 cx88sdr_hlock_get(struct cx88sdr_dev *dev)
{
	u32 status = cx88sdr_ioread(dev, CX88SDR_DEV_STAT);

	if (((status >> 5) & 3) == 3)
		return 1;
	return 0;
}

static void cx88sdr_gain_set(struct cx88sdr_dev *dev)
{
	cx88sdr_iowrite(dev, CX88SDR_AGC_ADJ3, ((u32)(dev->vctrl.agc_adj3 + 40) << 16) | 0x38c0);
	cx88sdr_iowrite(dev, CX88SDR_AGC_ADJ4, ((u32)dev->vctrl.gain_6db << 23) |
					       ((u32)dev->vctrl.gain << 16) | 0x2c34);
	cx88sdr_iowrite(dev, CX88SDR_AGC_TIP3, 0x1e48e000 | (u32)(64 - dev->vctrl.agc_tip3));
}

static void cx88sdr_input_set(struct cx88sdr_dev *dev)
{
	cx88sdr_iowrite(dev, CX88SDR_HTOTAL, (u32)dev->vctrl.htotal);
	cx88sdr_iowrite(dev, CX88SDR_PLL_ADJ_CTRL, ((u32)dev->vctrl.afc_pll << 25) | 0x0101f190);
	cx88sdr_iowrite(dev, CX88SDR_INPUT_FORMAT, 0x012010 | ((u32)dev->vctrl.input << 14) |
						   (u32)dev->vctrl.video_format);
}

static int cx88sdr_g_ctrl(struct v4l2_ctrl *ctrl)
{
	struct cx88sdr_dev *dev = container_of(ctrl->handler, struct cx88sdr_dev, ctrl_handler);

	switch (ctrl->id) {
	case V4L2_CID_CX88SDR_ADC_MODE:
		ctrl->val = cx88sdr_adc_mode_get(dev);
		break;
	case V4L2_CID_CX88SDR_HLOCK:
		ctrl->val = cx88sdr_hlock_get(dev);
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static int cx88sdr_s_ctrl(struct v4l2_ctrl *ctrl)
{
	struct cx88sdr_dev *dev = container_of(ctrl->handler, struct cx88sdr_dev, ctrl_handler);

	switch (ctrl->id) {
	case V4L2_CID_CX88SDR_INPUT:
		dev->vctrl.input = ctrl->val;
		cx88sdr_input_set(dev);
		break;
	case V4L2_CID_CX88SDR_VIDEO_FORMAT:
		dev->vctrl.video_format = ctrl->val;
		cx88sdr_input_set(dev);
		break;
	case V4L2_CID_CX88SDR_GAIN:
		dev->vctrl.gain = ctrl->val;
		cx88sdr_gain_set(dev);
		break;
	case V4L2_CID_CX88SDR_GAIN_6DB:
		dev->vctrl.gain_6db = !!ctrl->val;
		cx88sdr_gain_set(dev);
		break;
	case V4L2_CID_CX88SDR_AGC_TIP3:
		dev->vctrl.agc_tip3 = ctrl->val;
		cx88sdr_gain_set(dev);
		break;
	case V4L2_CID_CX88SDR_AGC_ADJ3:
		dev->vctrl.agc_adj3 = ctrl->val;
		cx88sdr_gain_set(dev);
		break;
	case V4L2_CID_CX88SDR_HTOTAL:
		dev->vctrl.htotal = ctrl->val;
		cx88sdr_input_set(dev);
		break;
	case V4L2_CID_CX88SDR_AFC_PLL:
		dev->vctrl.afc_pll = !!ctrl->val;
		cx88sdr_input_set(dev);
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static const struct v4l2_ctrl_ops cx88sdr_ctrl_ops = {
	.g_volatile_ctrl = cx88sdr_g_ctrl,
	.s_ctrl = cx88sdr_s_ctrl,
};

static const char * const cx88sdr_ctrl_input_menu_strings[] = {
	"1", "2", "3", "4", NULL
};

static const struct v4l2_ctrl_config cx88sdr_ctrl_input = {
	.ops	= &cx88sdr_ctrl_ops,
	.id	= V4L2_CID_CX88SDR_INPUT,
	.name	= "Input",
	.type	= V4L2_CTRL_TYPE_MENU,
	.max	= 3,
	.def	= cx88sdr_ctrl_mode[CX88SDR_CTRL_MODE].input,
	.qmenu	= cx88sdr_ctrl_input_menu_strings,
};

static const char * const cx88sdr_ctrl_adc_mode_menu_strings[] = {
	"SDR", "NTSC", "NTSC VCR", "PAL", "PAL VCR", NULL
};

static const struct v4l2_ctrl_config cx88sdr_ctrl_adc_mode = {
	.ops	= &cx88sdr_ctrl_ops,
	.id	= V4L2_CID_CX88SDR_ADC_MODE,
	.name	= "ADC Mode",
	.type	= V4L2_CTRL_TYPE_MENU,
	.max	= 4,
	.qmenu	= cx88sdr_ctrl_adc_mode_menu_strings,
	.flags	= V4L2_CTRL_FLAG_READ_ONLY | V4L2_CTRL_FLAG_VOLATILE,
};

static const char * const cx88sdr_ctrl_video_format_menu_strings[] = {
	"Autodetect", "NTSC-M", "NTSC-J", "NTSC-4.43", "PAL",
	"PAL-M", "Reserved", "PAL-N", "PAL-60", "SECAM", NULL
};

static const struct v4l2_ctrl_config cx88sdr_ctrl_video_format = {
	.ops	= &cx88sdr_ctrl_ops,
	.id	= V4L2_CID_CX88SDR_VIDEO_FORMAT,
	.name	= "Video Format",
	.type	= V4L2_CTRL_TYPE_MENU,
	.max	= 9,
	.def	= cx88sdr_ctrl_mode[CX88SDR_CTRL_MODE].video_format,
	.qmenu	= cx88sdr_ctrl_video_format_menu_strings,
	.menu_skip_mask = 64,
};

static const struct v4l2_ctrl_config cx88sdr_ctrl_gain = {
	.ops	= &cx88sdr_ctrl_ops,
	.id	= V4L2_CID_CX88SDR_GAIN,
	.name	= "Gain",
	.type	= V4L2_CTRL_TYPE_INTEGER,
	.max	= 31,
	.step	= 1,
	.def	= cx88sdr_ctrl_mode[CX88SDR_CTRL_MODE].gain,
	.flags	= V4L2_CTRL_FLAG_SLIDER,
};

static const struct v4l2_ctrl_config cx88sdr_ctrl_gain_6db = {
	.ops	= &cx88sdr_ctrl_ops,
	.id	= V4L2_CID_CX88SDR_GAIN_6DB,
	.name	= "Gain +6dB",
	.type	= V4L2_CTRL_TYPE_BOOLEAN,
	.max	= 1,
	.step	= 1,
	.def	= cx88sdr_ctrl_mode[CX88SDR_CTRL_MODE].gain_6db,
};

static const struct v4l2_ctrl_config cx88sdr_ctrl_agc_tip3 = {
	.ops	= &cx88sdr_ctrl_ops,
	.id	= V4L2_CID_CX88SDR_AGC_TIP3,
	.name	= "DC Offset",
	.type	= V4L2_CTRL_TYPE_INTEGER,
	.min	= -63,
	.max	= 64,
	.step	= 1,
	.def	= cx88sdr_ctrl_mode[CX88SDR_CTRL_MODE].agc_tip3,
	.flags	= V4L2_CTRL_FLAG_SLIDER,
};

static const struct v4l2_ctrl_config cx88sdr_ctrl_agc_adj3 = {
	.ops	= &cx88sdr_ctrl_ops,
	.id	= V4L2_CID_CX88SDR_AGC_ADJ3,
	.name	= "Sync Gain",
	.type	= V4L2_CTRL_TYPE_INTEGER,
	.min	= -16,
	.max	= 16,
	.step	= 1,
	.def	= cx88sdr_ctrl_mode[CX88SDR_CTRL_MODE].agc_adj3,
	.flags	= V4L2_CTRL_FLAG_SLIDER,
};

static const struct v4l2_ctrl_config cx88sdr_ctrl_htotal = {
	.ops	= &cx88sdr_ctrl_ops,
	.id	= V4L2_CID_CX88SDR_HTOTAL,
	.name	= "Line Width",
	.type	= V4L2_CTRL_TYPE_INTEGER,
	.min	= 1,
	.max	= 2047,
	.step	= 1,
	.def	= cx88sdr_ctrl_mode[CX88SDR_CTRL_MODE].htotal,
	.flags	= V4L2_CTRL_FLAG_SLIDER,
};

static const struct v4l2_ctrl_config cx88sdr_ctrl_hlock = {
	.ops	= &cx88sdr_ctrl_ops,
	.id	= V4L2_CID_CX88SDR_HLOCK,
	.name	= "HSync Lock",
	.type	= V4L2_CTRL_TYPE_BOOLEAN,
	.max	= 1,
	.step	= 1,
	.flags	= V4L2_CTRL_FLAG_READ_ONLY | V4L2_CTRL_FLAG_VOLATILE,
};

static const struct v4l2_ctrl_config cx88sdr_ctrl_afc_pll = {
	.ops	= &cx88sdr_ctrl_ops,
	.id	= V4L2_CID_CX88SDR_AFC_PLL,
	.name	= "PLL AFC",
	.type	= V4L2_CTRL_TYPE_BOOLEAN,
	.max	= 1,
	.step	= 1,
	.def	= cx88sdr_ctrl_mode[CX88SDR_CTRL_MODE].afc_pll,
};

static void cx88sdr_pci_latency_set(struct cx88sdr_dev *dev)
{
	u8 pci_lat, dev_lat = 248;

	if ((latency[dev->nr] >= 32) && (latency[dev->nr] < 248))
		dev_lat = (u8)round_up(latency[dev->nr], 8);
	pci_write_config_byte(dev->pdev, PCI_LATENCY_TIMER, dev_lat);
	pci_read_config_byte(dev->pdev, PCI_LATENCY_TIMER, &pci_lat);
	dev->pci_lat = (u32)pci_lat;
}

static void cx88sdr_shutdown(struct cx88sdr_dev *dev)
{
	cx88sdr_iowrite(dev, CX88SDR_DEV_CNTRL2, 0x00); /* Disable RISC Controller and IRQs */
	cx88sdr_iowrite(dev, CX88SDR_VID_DMA_CNTRL, 0x00); /* Stop DMA transfers */
	cx88sdr_iowrite(dev, CX88SDR_PCI_INT_MSK, CX88SDR_PCI_INT_MSK_CLEAR); /* Stop interrupts */
	cx88sdr_iowrite(dev, CX88SDR_VID_INT_MSK, CX88SDR_VID_INT_MSK_CLEAR);
	cx88sdr_iowrite(dev, CX88SDR_CAPTURE_CTRL, 0x00); /* Stop capturing */
	cx88sdr_iowrite(dev, CX88SDR_VID_INT_STAT, CX88SDR_VID_INT_STAT_CLEAR);
}

static void cx88sdr_bands_setup(struct cx88sdr_dev *dev)
{
	dev->xtal = CX88SDR_XTAL_FREQ;
	if ((xtal[dev->nr] >= CX88SDR_XTAL_FREQ_MIN) && (xtal[dev->nr] <= CX88SDR_XTAL_FREQ_MAX)) {
		dev->xtal = xtal[dev->nr];
		dev->vctrl.freq = (u32)dev->xtal;
	}

	dev->band[CX88SDR_BAND_RU08].type = V4L2_TUNER_SDR;
	dev->band[CX88SDR_BAND_RU08].capability = (V4L2_TUNER_CAP_1HZ | V4L2_TUNER_CAP_FREQ_BANDS);
	dev->band[CX88SDR_BAND_RU08].rangelow = CX88SDR_BAND_RANGE_LOW((u32)dev->xtal);
	dev->band[CX88SDR_BAND_RU08].rangehigh = CX88SDR_BAND_RANGE_HIGH((u32)dev->xtal);

	dev->band[CX88SDR_BAND_RU16].type = dev->band[CX88SDR_BAND_RU08].type;
	dev->band[CX88SDR_BAND_RU16].capability = dev->band[CX88SDR_BAND_RU08].capability;
	dev->band[CX88SDR_BAND_RU16].rangelow = dev->band[CX88SDR_BAND_RU08].rangelow / 2;
	dev->band[CX88SDR_BAND_RU16].rangehigh = dev->band[CX88SDR_BAND_RU08].rangehigh / 2;
}

static void cx88sdr_sram_setup(struct cx88sdr_dev *dev)
{
	u32 i;

	/* Write CDT */
	for (i = 0; i < CX88SDR_CDT_SIZE; i++)
		cx88sdr_iowrite(dev, CX88SDR_CDT_ADDR + 16 * i,
				CX88SDR_CLUSTER_BUF_ADDR + CX88SDR_VBI_PACKET_SIZE * i);

	/* Write CMDS */
	cx88sdr_iowrite(dev, CX88SDR_DMA24_CMDS_ADDR +  0, dev->risc_buf_addr);
	cx88sdr_iowrite(dev, CX88SDR_DMA24_CMDS_ADDR +  4, CX88SDR_CDT_ADDR);
	cx88sdr_iowrite(dev, CX88SDR_DMA24_CMDS_ADDR +  8, (CX88SDR_CDT_SIZE * 16) >> 3);
	cx88sdr_iowrite(dev, CX88SDR_DMA24_CMDS_ADDR + 12, CX88SDR_RISC_INST_QUEUE_ADDR);
	cx88sdr_iowrite(dev, CX88SDR_DMA24_CMDS_ADDR + 16, CX88SDR_RISC_INST_QUEUE_SIZE);

	/* Fill registers */
	cx88sdr_iowrite(dev, CX88SDR_DMA24_PTR2, CX88SDR_CDT_ADDR);
	cx88sdr_iowrite(dev, CX88SDR_DMA24_CNT1, (CX88SDR_VBI_PACKET_SIZE >> 3) - 1);
	cx88sdr_iowrite(dev, CX88SDR_DMA24_CNT2, (CX88SDR_CDT_SIZE * 16) >> 3);
}

static void cx88sdr_adc_setup(struct cx88sdr_dev *dev)
{
	cx88sdr_iowrite(dev, CX88SDR_VID_INT_STAT, cx88sdr_ioread(dev, CX88SDR_VID_INT_STAT));
	cx88sdr_iowrite(dev, CX88SDR_OUTPUT_FORMAT, 0x0e);
	cx88sdr_iowrite(dev, CX88SDR_COLOR_CTRL, 0xee);
	cx88sdr_iowrite(dev, CX88SDR_VBI_PACKET, (CX88SDR_VBI_PACKET_SIZE << 17) | 0x1000);
	cx88sdr_iowrite(dev, CX88SDR_AFE_CFG_IO, 0x12); /* Power off audio DAC+ADC */
	cx88sdr_iowrite(dev, CX88SDR_DEV_CNTRL2, 0x20);
	cx88sdr_iowrite(dev, CX88SDR_VID_DMA_CNTRL, 0x88); /* Start DMA */
}

static int cx88sdr_risc_buf_alloc(struct cx88sdr_dev *dev)
{
	dev->risc_buf = dma_alloc_coherent(&dev->pdev->dev, CX88SDR_RISC_BUF_SIZE,
					   &dev->risc_buf_addr, GFP_KERNEL | __GFP_ZERO);
	if (!dev->risc_buf)
		return -ENOMEM;
	return 0;
}

static void cx88sdr_risc_buf_free(struct cx88sdr_dev *dev)
{
	if (dev->risc_buf) {
		dma_free_coherent(&dev->pdev->dev, CX88SDR_RISC_BUF_SIZE,
				  dev->risc_buf, dev->risc_buf_addr);
		dev->risc_buf = NULL;
		dev->risc_buf_addr = (dma_addr_t)0;
	}
}

static int cx88sdr_dma_buf_alloc(struct cx88sdr_dev *dev)
{
	u32 frame;

	dev->dma_frames_addr = kcalloc(CX88SDR_VBI_DMA_FRAMES, sizeof(dma_addr_t), GFP_KERNEL);
	if (!dev->dma_frames_addr)
		return -ENOMEM;

	dev->dma_frames = kcalloc(CX88SDR_VBI_DMA_FRAMES, sizeof(void *), GFP_KERNEL);
	if (!dev->dma_frames)
		goto free_dma_frames_addr;

	for (frame = 0; frame < CX88SDR_VBI_DMA_FRAMES; frame++) {
		dma_addr_t dma_handle;

		dev->dma_frames[frame] = dma_alloc_coherent(&dev->pdev->dev,
							    CX88SDR_VBI_DMA_FRAME_SIZE,
							    &dma_handle, GFP_KERNEL | __GFP_ZERO);
		if (!dev->dma_frames[frame])
			goto free_dma_frames;
		dev->dma_frames_addr[frame] = dma_handle;
	}

	cx88sdr_pr_info("RISC: %u KiB, DMA: %u MiB, Buffer: %u KiB\n",
			(u32)(CX88SDR_RISC_BUF_SIZE / SZ_1K),
			(u32)(CX88SDR_VBI_DMA_AREA_SIZE / SZ_1M),
			(u32)(CX88SDR_VBI_DMA_FRAME_SIZE / SZ_1K));
	return 0;

free_dma_frames:
	while (frame) {
		frame--;
		dma_free_coherent(&dev->pdev->dev, CX88SDR_VBI_DMA_FRAME_SIZE,
				  dev->dma_frames[frame], dev->dma_frames_addr[frame]);
		dev->dma_frames[frame] = NULL;
		dev->dma_frames_addr[frame] = (dma_addr_t)0;
	}
	kfree(dev->dma_frames);
	dev->dma_frames = NULL;
free_dma_frames_addr:
	kfree(dev->dma_frames_addr);
	dev->dma_frames_addr = NULL;
	return -ENOMEM;
}

static void cx88sdr_dma_buf_free(struct cx88sdr_dev *dev)
{
	u32 frame;

	for (frame = 0; frame < CX88SDR_VBI_DMA_FRAMES; frame++) {
		if (dev->dma_frames[frame]) {
			dma_free_coherent(&dev->pdev->dev, CX88SDR_VBI_DMA_FRAME_SIZE,
					  dev->dma_frames[frame], dev->dma_frames_addr[frame]);
			dev->dma_frames[frame] = NULL;
			dev->dma_frames_addr[frame] = (dma_addr_t)0;
		}
	}
	kfree(dev->dma_frames);
	dev->dma_frames = NULL;
	kfree(dev->dma_frames_addr);
	dev->dma_frames_addr = NULL;
}

static void cx88sdr_risc_setup(struct cx88sdr_dev *dev)
{
	__le32 *risc_buf = dev->risc_buf;
	u32 frame, risc_loop_addr = dev->risc_buf_addr + CX88SDR_RISC_INST_SYNC_SIZE;

	*risc_buf++ = cpu_to_le32(CX88SDR_RISC_SYNC | CX88SDR_RISC_CNT_RESET);

	for (frame = 0; frame < CX88SDR_VBI_DMA_FRAMES; frame++) {
		u32 packet, dma_addr = dev->dma_frames_addr[frame];
		u32 risc_cnt_ctl = (frame < (CX88SDR_VBI_DMA_FRAMES - 1)) ?
				   CX88SDR_RISC_CNT_INCR : CX88SDR_RISC_CNT_RESET;

		for (packet = 0; packet < (CX88SDR_VBI_PACKETS_PER_FRAME - 1); packet++) {
			*risc_buf++ = cpu_to_le32(CX88SDR_RISC_WRITE_VBI_PACKET);
			*risc_buf++ = cpu_to_le32(dma_addr);
			dma_addr += CX88SDR_VBI_PACKET_SIZE;
		}
		*risc_buf++ = cpu_to_le32(CX88SDR_RISC_WRITE_VBI_PACKET | risc_cnt_ctl);
		*risc_buf++ = cpu_to_le32(dma_addr);
	}
	*risc_buf++ = cpu_to_le32(CX88SDR_RISC_JUMP);
	*risc_buf++ = cpu_to_le32(risc_loop_addr);
}

static irqreturn_t cx88sdr_irq(int __always_unused irq, void *dev_id)
{
	struct cx88sdr_dev *dev = dev_id;
	int i, handled = 0;
	u32 mask, status;

	for (i = 0; i < 10; i++) {
		status = cx88sdr_ioread(dev, CX88SDR_VID_INT_STAT);
		mask = cx88sdr_ioread(dev, CX88SDR_VID_INT_MSK);
		if (!(status & mask))
			break;
		cx88sdr_iowrite(dev, CX88SDR_VID_INT_STAT, status);
		handled = 1;
	}
	return IRQ_RETVAL(handled);
}

static int cx88sdr_probe(struct pci_dev *pdev, const struct pci_device_id __always_unused *pci_id)
{
	struct cx88sdr_dev *dev;
	struct v4l2_device *v4l2_dev;
	struct v4l2_ctrl_handler *hdl;
	int ret;

	if (cx88sdr_devcount >= CX88SDR_MAX_CARDS)
		return -ENODEV;

	ret = pci_enable_device(pdev);
	if (ret)
		return ret;

	pci_set_master(pdev);

	if (dma_set_mask(&pdev->dev, DMA_BIT_MASK(32))) {
		dev_err(&pdev->dev, "no suitable DMA support available\n");
		ret = -EFAULT;
		goto disable_device;
	}

	dev = devm_kzalloc(&pdev->dev, sizeof(*dev), GFP_KERNEL);
	if (!dev) {
		ret = -ENOMEM;
		dev_err(&pdev->dev, "can't allocate memory\n");
		goto disable_device;
	}

	dev->nr = cx88sdr_devcount;
	dev->pdev = pdev;
	snprintf(dev->name, sizeof(dev->name), CX88SDR_DRV_NAME " [%d]", dev->nr);

	cx88sdr_pci_latency_set(dev);

	ret = pci_request_regions(pdev, KBUILD_MODNAME);
	if (ret) {
		cx88sdr_pr_err("can't request memory regions\n");
		goto disable_device;
	}

	ret = cx88sdr_risc_buf_alloc(dev);
	if (ret) {
		cx88sdr_pr_err("can't alloc RISC buffer\n");
		goto free_pci_regions;
	}

	ret = cx88sdr_dma_buf_alloc(dev);
	if (ret) {
		cx88sdr_pr_err("can't alloc DMA buffer\n");
		goto free_risc_inst_buffer;
	}

	cx88sdr_risc_setup(dev);

	dev->ctrl = pci_ioremap_bar(pdev, 0);
	if (!dev->ctrl) {
		ret = -ENOMEM;
		cx88sdr_pr_err("can't ioremap Control BAR\n");
		goto free_dma_buffer;
	}

	cx88sdr_shutdown(dev);
	cx88sdr_sram_setup(dev);

	ret = request_irq(pdev->irq, cx88sdr_irq, IRQF_SHARED, KBUILD_MODNAME, dev);
	if (ret) {
		cx88sdr_pr_err("failed to request IRQ\n");
		goto free_ctrl;
	}

	dev->irq = pdev->irq;
	synchronize_irq(dev->irq);

	/* Set initial values */
	dev->vctrl = cx88sdr_ctrl_mode[CX88SDR_CTRL_MODE];
	cx88sdr_bands_setup(dev);

	cx88sdr_adc_setup(dev);
	ret = cx88sdr_adc_fmt_set(dev);
	if (ret) {
		cx88sdr_pr_err("failed to config ADC\n");
		goto free_irq;
	}

	cx88sdr_gain_set(dev);
	cx88sdr_input_set(dev);

	mutex_init(&dev->vdev_mlock);
	mutex_init(&dev->vopen_mlock);

	v4l2_dev = &dev->v4l2_dev;
	ret = v4l2_device_register(&pdev->dev, v4l2_dev);
	if (ret) {
		v4l2_err(v4l2_dev, "can't register V4L2 device\n");
		goto free_irq;
	}

	hdl = &dev->ctrl_handler;
	v4l2_ctrl_handler_init(hdl, 10);
	v4l2_ctrl_new_custom(hdl, &cx88sdr_ctrl_input, NULL);
	v4l2_ctrl_new_custom(hdl, &cx88sdr_ctrl_adc_mode, NULL);
	v4l2_ctrl_new_custom(hdl, &cx88sdr_ctrl_video_format, NULL);
	v4l2_ctrl_new_custom(hdl, &cx88sdr_ctrl_gain, NULL);
	v4l2_ctrl_new_custom(hdl, &cx88sdr_ctrl_gain_6db, NULL);
	v4l2_ctrl_new_custom(hdl, &cx88sdr_ctrl_agc_tip3, NULL);
	v4l2_ctrl_new_custom(hdl, &cx88sdr_ctrl_agc_adj3, NULL);
	v4l2_ctrl_new_custom(hdl, &cx88sdr_ctrl_htotal, NULL);
	v4l2_ctrl_new_custom(hdl, &cx88sdr_ctrl_hlock, NULL);
	v4l2_ctrl_new_custom(hdl, &cx88sdr_ctrl_afc_pll, NULL);
	v4l2_dev->ctrl_handler = hdl;
	if (hdl->error) {
		ret = hdl->error;
		v4l2_err(v4l2_dev, "can't register V4L2 controls\n");
		goto free_v4l2;
	}

	/* Initialize the video_device structure */
	strscpy(v4l2_dev->name, dev->name, sizeof(v4l2_dev->name));
	dev->vdev = cx88sdr_vdev;
	dev->vdev.ctrl_handler = &dev->ctrl_handler;
	dev->vdev.lock = &dev->vdev_mlock;
	dev->vdev.v4l2_dev = v4l2_dev;
	video_set_drvdata(&dev->vdev, dev);

	ret = video_register_device(&dev->vdev, VFL_TYPE_SDR, -1);
	if (ret)
		goto free_v4l2;

	cx88sdr_pr_info("IRQ: %u, Control: %pR, PCI latency: %u, Xtal: %u Hz\n",
			dev->pdev->irq, &dev->pdev->resource[0], dev->pci_lat, (u32)dev->xtal);
	cx88sdr_pr_info("registered as %s\n", video_device_node_name(&dev->vdev));
	cx88sdr_iowrite(dev, CX88SDR_VID_INT_MSK, CX88SDR_VID_INT_MSK_VAL);
	cx88sdr_devcount++;
	return 0;

free_v4l2:
	v4l2_ctrl_handler_free(hdl);
	v4l2_device_unregister(v4l2_dev);
free_irq:
	free_irq(dev->irq, dev);
free_ctrl:
	iounmap(dev->ctrl);
free_dma_buffer:
	cx88sdr_dma_buf_free(dev);
free_risc_inst_buffer:
	cx88sdr_risc_buf_free(dev);
free_pci_regions:
	pci_release_regions(pdev);
disable_device:
	pci_disable_device(pdev);
	return ret;
}

static void cx88sdr_remove(struct pci_dev *pdev)
{
	struct v4l2_device *v4l2_dev = pci_get_drvdata(pdev);
	struct cx88sdr_dev *dev = container_of(v4l2_dev, struct cx88sdr_dev, v4l2_dev);

	cx88sdr_shutdown(dev);
	cx88sdr_pr_info("removing %s\n", video_device_node_name(&dev->vdev));
	cx88sdr_devcount--;
	video_unregister_device(&dev->vdev);
	v4l2_ctrl_handler_free(&dev->ctrl_handler);
	v4l2_device_unregister(&dev->v4l2_dev);
	free_irq(dev->irq, dev);
	iounmap(dev->ctrl);
	cx88sdr_dma_buf_free(dev);
	cx88sdr_risc_buf_free(dev);
	pci_release_regions(pdev);
	pci_disable_device(pdev);
}

static int __maybe_unused cx88sdr_suspend(struct device *dev_d)
{
	struct v4l2_device *v4l2_dev = dev_get_drvdata(dev_d);
	struct cx88sdr_dev *dev = container_of(v4l2_dev, struct cx88sdr_dev, v4l2_dev);

	cx88sdr_shutdown(dev);
	return 0;
}

static int __maybe_unused cx88sdr_resume(struct device *dev_d)
{
	struct v4l2_device *v4l2_dev = dev_get_drvdata(dev_d);
	struct cx88sdr_dev *dev = container_of(v4l2_dev, struct cx88sdr_dev, v4l2_dev);
	int ret;

	cx88sdr_shutdown(dev);
	cx88sdr_sram_setup(dev);
	cx88sdr_adc_setup(dev);
	ret = cx88sdr_adc_fmt_set(dev);
	if (ret)
		return ret;
	cx88sdr_gain_set(dev);
	cx88sdr_input_set(dev);
	cx88sdr_iowrite(dev, CX88SDR_VID_INT_MSK, CX88SDR_VID_INT_MSK_VAL);
	mutex_lock(&dev->vopen_mlock);
	if (dev->vopen)
		cx88sdr_iowrite(dev, CX88SDR_PCI_INT_MSK, CX88SDR_PCI_INT_MSK_VAL);
	mutex_unlock(&dev->vopen_mlock);
	return 0;
}

static const struct pci_device_id cx88sdr_pci_tbl[] = {
	{ PCI_DEVICE(0x14f1, 0x8800) },
	{ }
};
MODULE_DEVICE_TABLE(pci, cx88sdr_pci_tbl);

static SIMPLE_DEV_PM_OPS(cx88sdr_pm_ops, cx88sdr_suspend, cx88sdr_resume);

static struct pci_driver cx88sdr_pci_driver = {
	.name		= KBUILD_MODNAME,
	.id_table	= cx88sdr_pci_tbl,
	.probe		= cx88sdr_probe,
	.remove		= cx88sdr_remove,
	.driver.pm	= &cx88sdr_pm_ops,
};

module_pci_driver(cx88sdr_pci_driver);
